---
layout: post
title: "Kitchen-Sink'"
tags: #designthinking, #dev, #healthcareIT
image: /assets/sinkdishes.jpg
comments: True
draft: true
---
### "I was viscerally offended"

I ran across this gem of a video from a past Çingleton 2011 conference[^1] while perusing for inspiring design thinking over the weekend. This excerpt by acclaimed Apple aficionado John Gruber (<a href="http://twitter.com/gruber">@gruber</a>) is a brutal teardown of how practice management software, in his example dental software, and to an extension EMR software is seen by the outside world.  As part of the healthcare IT industry, we already know how bad the design decisions made by legacy healthcare industry vendors are but we rarely get this level of public critique from an outsider looking in.

To hear it publicly from those that are far removed from healthcare is a reality check and actually refreshing in a way to hopefully disenchant the industry into changing its ways. At healthcare conferences we the IT collective often gloss over how bad software is to a point where we've become numb to it. Gruber is "viscerally offended". Strong words and yet entirely apt and broadly deserved.

### "The software seems out of place"
<iframe width="560" height="315" src="//www.youtube.com/embed/_wCbqtlKpQI" frameborder="0" allowfullscreen></iframe>
In this excerpt Gruber dives into the particulars of design flaws in data entry screens. The misalignment, the excessive mouse clicks, the lack of a screen information heirchy. At a bird's eye view of thinking, Gruber points out that medical software is out of touch with not only current technology such as touch enabled devices, but completely misses the mark of basic human centered interface. That the front desk medical assistant has to arduously complete basic tasks because of bad design is one thing, but think about how seconds add up.

Gruber isn't an outsider, hhe's by default a patient/consumer and a parent of a child observing a typical office visit. Yes, his 'like any good nerd' intellect for curiosity and ability to convey it are above theconsume but his experience is the same as with all patients. As an industry, we know that healthcare is known as the bastion of bad design. These screens are in our faces every hour. But I'm more interested though in why the culture of healthcare has carried this stigma for so long; let's count decades. 


### We lack a cultural fortitude to say "No"

On the surface I see this as classic textbook complacency by decision makers that allow this kind of software to ship version after version. Healthcare IT tends to throw every feature requested at the screen. And it can be argued that most enterprise companies do the same. I'll grant that. But the difference is that in healthcare, the 'more' is rewarded directly and indirectly. The difference being a culture of detrimental inclusion. That is, due to the nature of the product category 'medical health', the fear of malpractice, litigation and medcial competancy a 'better safe than sorry' culture is esphed everywhere. More MRIs, more <a href="http://www.bmj.com/content/348/bmj.g366">mammograms</a>, more prescriptions, mores examinations spews into 'more is better' thinking. So heck why not more software features? Isn't that a good thing? 

No. It's precisely the lack of discipline in healthcare software that leads us to burdensome screen information overload. Things get <a href="http://archinte.jamanetwork.com/article.aspx?articleid=1657753">missed</a> not because of missing features, rather a lack of a coherant design thinking. Our current inclinations are to pack as much functionality at the cost of great end user experiences. We need a value shift of form <i>over</i>> function. That means having to say 'No' to adding things for the sake of group thinking inclusiveness. 

###Why saying "No" is hard 

>"Design almost invariably involves compromise." --Bryan Lawson

To understand the reasons behind current state of healthcare software, you have to examine how healthcare software is developed and sold to various constituents. By sold I mean buy-in from product manager, medical advisory boards and the growing trend of patient advocates. 

A software vendor internally stratergizes what they'd like to build anew or increment on. Here the decision maker within the company is more controlled. Whether it's the marketing department driving the latest buzzwords like 'more synergy', 'collaborative experience' or of late 'patient centric'[^2] or the operations and finance departments simply needing more revenue streams, features get encapsulated and packaged into a ship-able release. Within a company compromises are trade off from the old "cheap, quick, good : pick any two" adage. Someone has had to say 'No' at some point for it to eventually get released.

Then sales staff from internal sales to external partners and VARs meet and greet hospitals, group practices and small solo practitioners in a bid to convert sales.
This is where healthcare enterprise starts to differ from other verticals. This culture permeates the This is a misguided trail  to Purchasing decisions are driven by a multitude of decisions, the purchaser, either a large hospital or a solo physician's office gets to decide on features... 
###"What did you say 'No' to today?"

----
[^1]: Due to an autoplay timestamp bug in Vimeo, this video was re-coded for Youtube playback. The original Çingleton conference video in its entirety can be viewed [here](http://vimeo.com/31926572).
[^2]: Patient co-designed systems are a relatively new and promising trend but have yet to [prove market viability](https://www.sharedcareplan.org). In its early days it's still unclear if companies are seeing benefits or simply using it for marketing purposes.

{% include twitter_plug.html %}
