---
layout: post
title: "Engaged Invention"
tags: #mhealth, #google, #diabetes, #design
image: /assets/smart-contact-lenses.jpg
draft: false
---
Great [post](http://gigaom.com/2014/01/17/one-diabetics-take-on-googles-smart-contact-lenses/) by [@om](http://twitter.com/om), editor-in-chief of GigaOm on Google's [newest endeavor](http://googleblog.blogspot.com/2014/01/introducing-our-smart-contact-lens.html), contact lenses embedded with sensors to measure glucose in tears. Om points to research that contact lenses can [cause complications](http://www.ncbi.nlm.nih.gov/pubmed/22537249) for diabetic patients. He'd rather see something [non-invasive](http://www.diabetesmine.com/2012/11/non-invasive-diabetes-technology-still-dreaming.html).

The response to the lenses I thought was appropriate and had weight as he's a person with diabetes (PWD).[^1] I've followed his writings for years and never knew.

His main argument spoke to the tech crowd at large but I thought was right to the point of what healthcare IT is trying to go through:

> But after the initial excitement was over, cold reality set in. It also prompted me to ask the question: why is it that a company with such good intentions fails to ask itself very basic of questions, something a normal human being would ponder before embarking on a scientific quest?

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@cascadia.jpg" />
RT <a href="https://twitter.com/fqure">@fqure</a>: Tech editor <a href="https://twitter.com/om">@om</a> debunks Google&#39;s contact lenses as only an <a href="https://twitter.com/search?q=%23epatient&amp;src=hash">#epatient</a> himself can <a href="http://t.co/vG5DMmimMe">http://t.co/vG5DMmimMe</a> ^design for users needs<br>&mdash; Sherry Reynolds (@Cascadia) <a href="https://twitter.com/Cascadia/statuses/424798574447046656">January 19, 2014</a></blockquote>

Then some PWD e-Patients weighed in as well.

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@bigcheeseanders.jpg" />
<a href="https://twitter.com/DiabetesMine">@DiabetesMine</a> <a href="https://twitter.com/Cascadia">@Cascadia</a> <a href="https://twitter.com/fqure">@fqure</a> <a href="https://twitter.com/om">@om</a> I disagree fully. As a type1 of 7yrs with CGM i&#39;d love this to succeed. Todaya CGM is crap<br>&mdash; Anders Åberg (@BigCheeseAnders) <a href="https://twitter.com/BigCheeseAnders/statuses/424858643197353984">January 19, 2014</a></blockquote>


Developing anything for someone else's medical condition is incredibly rewarding but at the same time you're gripped with the sense that you will never really understand what they have. And that the closest experience to <i>getting it</i> is just plain listening and observing in the utmost awe of what he/she goes through, every hour.

It's unfair to completely dismiss what Google wants done. It's touched a chord with those that need an easier way to get through life. But the counterpoint is that Google does so much, with a high degree of turnover, that it gets viewed as tech for tech's sake.

Whether software or hardware, we still do not have a development methodology that takes into account healthcare social media as part of the [development cycle](http://en.wikipedia.org/wiki/Systems_development_life-cycle). Healthcare 'requirements gathering' for me is to keep listening through the use of the best free, widely adopted, tool we have today: social media.

We ask that a company the size and scope of Google adhere to the principles of real patient engagement. They can set the tone for a new era of engaged invention.

-----
[^1]: I first learned of the terms PWD and PWOD (persons without diabetes) from following the weekly diabetes #dsma twitter chats. I'd like to think it could also apply to Persons With Diseases/Disabilities. 


