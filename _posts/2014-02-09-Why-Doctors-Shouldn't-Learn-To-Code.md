---
layout: post
title: "Why Doctors Shouldn't Learn to Code"
tags: #designthinking
image: /assets/14-jan/million-mistake.jpg
draft: true
---
And by code I don't mean ICD-10, I mean software coding. I recently ran across a situation working with a team of software developers and designers tasked to work on a healthcare software project. Theit expereicen in healthcare ranged from limited to none.

We went through the usual corse of development methodolgies, starting with building user personas. There was a huge ommission in these user roles. Sets were made of all of the healthcare workers involved except the patient.

Development in other areas are quite straightforward. Like the financial industry, retail or food industy, software is either built for the end user, as in the consumer, or the enterprise client. Healthcare IT, for the most part, is a hybrid of the two where even if a system isn't built for direct patient interaction, modern development methods consider the patients. I could see that the patient as a user is sometimes blind and not clearly obvious.

### Healthcare Is Not Obvious
Developers think not only in logical constructs, but much of their code can apply to so many other areas, the concept of re-use. This means that for a developer, code is code. That there are very few code sets specifically that only apply to healthcare, usually in Take user authentication. Creating a login system is commonplace in all area of software and in healthcare it's 


### Healthcare Is Not Obvious

There is the thought 'Gee, if only I could learn to code then I could fix this mess.' 


The article was shared on Facebook over 10K times and a few hundred tweets. Several physicians however did raise concerns about the article:

<blockquote class="twitter-tweet" lang="en"><img src="/assets/avi/@joyclee.jpg" />.<a href="https://twitter.com/jkolko">@jkolko</a> (1/4) never thought about <a href="https://twitter.com/search?q=%23design&amp;src=hash">#design</a> this way; my perspective has been that as MDs/health systems our goal is to &mdash;Doctor as Designer (@joyclee)<a href="https://twitter.com/joyclee/statuses/432367363552714752">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en"><img src="/assets/avi/@joyclee.jpg" />.<a href="https://twitter.com/jkolko">@jkolko</a> (2/4) help pts achieve better health, but the tools we use in <a href="https://twitter.com/search?q=%23healthcare&amp;src=hash">#healthcare</a> are so ineffective and badly designed &mdash;Doctor as Designer (@joyclee) <a href="https://twitter.com/joyclee/statuses/432367414081503232">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en"><img src="/assets/avi/@joyclee.jpg" />.<a href="https://twitter.com/jkolko">@jkolko</a> (3/4) that it&#39;s a system engineered for failure and poor outcomes &mdash;Doctor as Designer (@joyclee) <a href="https://twitter.com/joyclee/statuses/432367458687930368">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en"><img src="/assets/avi/@joyclee.jpg" />.<a href="https://twitter.com/jkolko">@jkolko</a> (4/4) we need better designed technology systems to guide behavior, but I recognize the perils of this as well &mdash;Doctor as Designer (@joyclee) <a href="https://twitter.com/joyclee/statuses/432367496818343936">February 9, 2014</a></blockquote>

&nbsp;

### Click bait in action

#### The Headline
Let's begin with the $1 million claim. The author [Kathy Kristof](https://twitter.com/KathyKristof) a financial journalist, takes a $185K medical school debt and then amortizes it over 30 years to where it would balloon to $420K. What doctor takes 30 years to pay off medical school? With that logic, by the time of close to retirement age the debt would still be outstanding.
I get that the headline is meant to grab the reader, but it's just plain misleading to onlookers that don't get past the 6th paragraph.

The reader is left pondering with how the remaining half million becomes personal debt. It's implied that by comparing other specialities to primary care, the superficial tone is one of 'you made the wrong decision because some other surgeon is happier and richer than you are.' Untangible opportunity cost in economists lingo.

#### The Report
The article points to a [NerdWallet report](http://www.nerdwallet.com/blog/health/2014/01/17/primary-care-shortage-obamacare-physician-salary-specialty/) that links to an anti-Obamacare article.[^2] Political affiliations aside, the survey is detailed with salary breakdowns and citations. I'll let you be the judge on whether the physician shortage is Obama's fault or a decades long focus on procedures versus outcomes.

#### The Embedded Video 
Again, more click bait. The video is actually about patient engagement and has nothing to support the argument that choosing a doctorate in medicine is a financial pitfall as Kristof purports.

#### The Comments
I respect news sites that continue the conversation with their readers. Where every once in a while the author responds to questions or comments from its readership. This CBS article is crickets. Are they really interested in lively discourse or eyeballs to appease advertisers?

There are real consequences to chipping away at intelectual pursuits from the pulpit of the media. Like doubt and unrest from medical students like this one from the article's comment section:

> HELLOJESS22 January 19, 2014 11:11AM As a current med student, this scares me by how accurate it is. After spending two years being stuck in a classroom, I was so excited to get into a hospital. 

What if this line of thinking trickles down to parents that hesitate to send their kids to medical school? The primary care shortage gets worse. Specialists that rely on primary care for referrals are affected as well. And patients wait longer due to scarcity of resources.

### Wise choices
The only way to combat negative media is to reframe the conversation. To reiterate why doctors <i>really</i> get into medicine:

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@SeattleMamaDoc.jpg" />
I wholeheartedly disagree. Providing care &amp; reassurance to pts =v valuable currency <br>&mdash; WendySueSwanson MD (@SeattleMamaDoc)<br><a href="https://twitter.com/SeattleMamaDoc/statuses/426387997353725952">January 23, 2014</a></blockquote>

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@TheDocSmitty.jpg" />
<a href="https://twitter.com/SeattleMamaDoc">@SeattleMamaDoc</a> This article is so focused on the finances. Nothing as to other rewards of med or the view of the profession as a calling.<br>&mdash; Justin Smith (@TheDocSmitty) <a href="https://twitter.com/TheDocSmitty/statuses/426406205452341248">January 23, 2014</a></blockquote>

And further, Dr. Smith sums it up like a financial journalist should have:

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@TheDocSmitty.jpg" />
<a href="https://twitter.com/SeattleMamaDoc">@SeattleMamaDoc</a> Plus my experience is that the finances aren&#39;t that grim especially if you <b>make wise choices</b> regarding undergrad debt.<br>&mdash; Justin Smith (@TheDocSmitty) <a href="https://twitter.com/TheDocSmitty/statuses/426406305360666624">January 23, 2014</a></blockquote>


For physicians, the article does attempt to give a heads-up and get a rough feel for the lay of the land, even if it's on an economic comparison basis of the disgruntled. But debt is just a milestone to overcome, not an end all. A bean counter isn't worth a bean if they can't help find a way out of that debt. It's all in the outlook you plan to take and the wisdom you gain.

-----
[^1]: This CBS article is a rerun (pun intended) and was originally posted September 10, 2013
[^2]: The survey's link in the article is broken and after some serching I found it. I haven't gotten a [response](https://twitter.com/fqure/status/426364718841102336) from the author yet.


