---
layout: post
title: "Mistaken Million"
tags: #fmrevolution, #healthcare, #primarycare
image: /assets/14-jan/million-mistake.jpg
draft: false
---
The right wisdom can change lives and shift entire industries. We're in the midst of a fragile healthcare transition where reimbursement models and capacity to treat patients are being stressed. The consensus is that the front lines of our healthcare system, primary & family care physicians, are being [stretched thin](/assets/14-jan/MHE2013_SOI4.jpg).

Even with the day to day long hours, shrinking reimbursements and for some the debt load from years of medical school, those that stay on consider primary care a calling. Or at the very least a noble pursuit that can earn financial stability.  

It's the notion that hard work can reward you in a career that provides a stable livelihood that generations of students aspire to become. It's the bedrock of modern medicine. Given all the obstacles, is it a wise choice to become a doctor?


### Open season on doctors
A recent[^1] CBS MarketWatch article argues it's a mistake at the loss of [$1 million dollars](http://www.cbsnews.com/news/1-million-mistake-becoming-a-doctor) per physician.

How does the media get to jump to such conclusions? What's behind this [Dr. bashing](https://twitter.com/hjluks/status/426505334057074688) agenda? Media outlets try and change our perceptions in the pursuit of page views. We've seen it before. The more controversial, the more clicks, the more advertisers benefit.

The article was shared on Facebook over 10K times and a few hundred tweets. Several physicians however did raise concerns about the article:

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@Asthma3Ways.jpg" />
MDs I know don&#39;t agree. MDs wanting &gt;$ go into industry successfully. <a href="https://twitter.com/fqure">@fqure</a><br>&mdash; Ann Wu MD MPH (@Asthma3Ways)<br><a href="https://twitter.com/Asthma3Ways/statuses/426514287213805569">January 24, 2014</a></blockquote>

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@hjluks.jpg" />
Shame on CBS.. Their bizarre article was to push some form of agenda. Their conclusion.. If there was one, did...
<br>&mdash; Howard Luks MD (@hjluks) <a href="https://twitter.com/hjluks/statuses/426503157590392833">January 23, 2014</a></blockquote>

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@YoniFreedhoff.jpg" />
<a href="https://twitter.com/fqure">@fqure</a> Dunno. I graduated w/6 figure debt. Wasn&#39;t fun. Certainly important for people to appreciate going in. I sure didn&#39;t.<br>&mdash; Yoni Freedhoff, M.D. (@YoniFreedhoff) <a href="https://twitter.com/YoniFreedhoff/statuses/426414658736881665">January 23, 2014</a></blockquote>

<blockquote class="twitter-tweet" data-cards="hidden" lang="en">
<img src="/assets/avi/@subatomicdoc.jpg" />
It&#39;s about healing, but it&#39;s getting harder. Still worth it.<br>&mdash; Matthew Katz (@subatomicdoc)<br><a href="https://twitter.com/subatomicdoc/statuses/426394345408057344">January 23, 2014</a></blockquote>
&nbsp;

### Click bait in action

#### The Headline
Let's begin with the $1 million claim. The author [Kathy Kristof](https://twitter.com/KathyKristof) a financial journalist, takes a $185K medical school debt and then amortizes it over 30 years to where it would balloon to $420K. What doctor takes 30 years to pay off medical school? With that logic, by the time of close to retirement age the debt would still be outstanding.
I get that the headline is meant to grab the reader, but it's just plain misleading to onlookers that don't get past the 6th paragraph.

The reader is left pondering with how the remaining half million becomes personal debt. It's implied that by comparing other specialities to primary care, the superficial tone is one of 'you made the wrong decision because some other surgeon is happier and richer than you are.' Untangible opportunity cost in economists lingo.

#### The Report
The article points to a [NerdWallet report](http://www.nerdwallet.com/blog/health/2014/01/17/primary-care-shortage-obamacare-physician-salary-specialty/) that links to an anti-Obamacare article.[^2] Political affiliations aside, the survey is detailed with salary breakdowns and citations. I'll let you be the judge on whether the physician shortage is Obama's fault or a decades long focus on procedures versus outcomes.

#### The Embedded Video 
Again, more click bait. The video is actually about patient engagement and has nothing to support the argument that choosing a doctorate in medicine is a financial pitfall as Kristof purports.

#### The Comments
I respect news sites that continue the conversation with their readers. Where every once in a while the author responds to questions or comments from its readership. This CBS article is crickets. Are they really interested in lively discourse or eyeballs to appease advertisers?

There are real consequences to chipping away at intelectual pursuits from the pulpit of the media. Like doubt and unrest from medical students like this one from the article's comment section:

> HELLOJESS22 January 19, 2014 11:11AM As a current med student, this scares me by how accurate it is. After spending two years being stuck in a classroom, I was so excited to get into a hospital. 

What if this line of thinking trickles down to parents that hesitate to send their kids to medical school? The primary care shortage gets worse. Specialists that rely on primary care for referrals are affected as well. And patients wait longer due to scarcity of resources.

### Wise choices
The only way to combat negative media is to reframe the conversation. To reiterate why doctors <i>really</i> get into medicine:

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@SeattleMamaDoc.jpg" />
I wholeheartedly disagree. Providing care &amp; reassurance to pts =v valuable currency <br>&mdash; WendySueSwanson MD (@SeattleMamaDoc)<br><a href="https://twitter.com/SeattleMamaDoc/statuses/426387997353725952">January 23, 2014</a></blockquote>

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@TheDocSmitty.jpg" />
<a href="https://twitter.com/SeattleMamaDoc">@SeattleMamaDoc</a> This article is so focused on the finances. Nothing as to other rewards of med or the view of the profession as a calling.<br>&mdash; Justin Smith (@TheDocSmitty) <a href="https://twitter.com/TheDocSmitty/statuses/426406205452341248">January 23, 2014</a></blockquote>

And further, Dr. Smith sums it up like a financial journalist should have:

<blockquote class="twitter-tweet" lang="en">
<img src="/assets/avi/@TheDocSmitty.jpg" />
<a href="https://twitter.com/SeattleMamaDoc">@SeattleMamaDoc</a> Plus my experience is that the finances aren&#39;t that grim especially if you <b>make wise choices</b> regarding undergrad debt.<br>&mdash; Justin Smith (@TheDocSmitty) <a href="https://twitter.com/TheDocSmitty/statuses/426406305360666624">January 23, 2014</a></blockquote>


For physicians, the article does attempt to give a heads-up and get a rough feel for the lay of the land, even if it's on an economic comparison basis of the disgruntled. But debt is just a milestone to overcome, not an end all. A bean counter isn't worth a bean if they can't help find a way out of that debt. It's all in the outlook you plan to take and the wisdom you gain.

-----
[^1]: This CBS article is a rerun (pun intended) and was originally posted September 10, 2013
[^2]: The survey's link in the article is broken and after some serching I found it. I haven't gotten a [response](https://twitter.com/fqure/status/426364718841102336) from the author yet.


